#!/bin/bash

set -e
# Deploy script over ssh for our debian package.

VERSION=$(cat Cargo.toml | grep ^version | awk '{gsub(/"/, "", $3); print $3}')
CODE_VERSION=$(cat src/main.rs | grep '^const VERSION' | awk '{gsub(/("|;|\\n)/, "", $6); print $6}')

if [[ "$VERSION" != "$CODE_VERSION" ]]; then
  echo "version mismatch" && exit 1
fi

cargo deb

PACKAGE="chemist-runner_${VERSION}_amd64.deb"
scp "target/debian/${PACKAGE}" caeser:~
ssh caeser "sudo dpkg -i $PACKAGE && rm *.deb"
