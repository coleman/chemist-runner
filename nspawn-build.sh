#!/bin/bash

# This script demonstrates building redox with systemd-nspawn containers.
#
# Requirements: debootstrap, systemd-nspawn, 20GB of disk space
#
# You should run this as root right now. However, nspawn does have the ability to use
# Linux user namespaces, which might let us avoid that for some things.
#
# 1. Use debootstrap to create a base debian OS directory base dir.
# 2. systemd-nspawn (with the --template param) clones the OS tree into a checkout dir.
# 3. Several containerized systemd-nspawn commands are executed "inside" the checkout dir:
#     a. create a non-root user
#     b. install debian packages
#     c. install rust and rust tools
#     d. git clone redox
#     e. set up /etc/mtab, which nspawn doesn't do for us
# 4. Finally, the checkout dir is itself cloned for an invocation of `make all`
#
# WARNING: This build strategy will take up at least 20 GB of space, maybe more. This is because
# the large checkouts of redox are copied. The idea behind this approach is to provide clean,
# isolated build environments, but to minimize what is fetched over the network.
#
# There is even more we could potentially cache. Systemd-nspawn gives us the ability to
# manipulate the network interfaces and DNS settings of the environment. With some (lots of?) effort, we
# could cache HTTP requests coming from within the build: wget and cargo are the big offenders
# as far as I can tell.

set -e

BUILD_USER="builder"
BASE_DIR="debian-stable"
CHECKOUT_DIR="redox-master"
BUILD_DIR="redox-build"
CRATES_DIR="crates-mirror"

# create debootstrap chroot
if [[ ! -d $BASE_DIR ]]; then
  debootstrap stable $BASE_DIR
fi

systemd-nspawn -D $CRATES_DIR --template $BASE_DIR --as-pid2 useradd -G sudo -m -s /bin/bash $BUILD_USER
systemd-nspawn -D $CRATES_DIR --as-pid2 apt-get install -y curl build-essential libsqlite3-dev sudo dbus
systemd-nspawn -D $CRATES_DIR --as-pid2 -u $BUILD_USER --chdir=/home/$BUILD_USER \
  sh -c 'curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly'
systemd-nspawn -D $CRATES_DIR --as-pid2 -u $BUILD_USER /home/$BUILD_USER/.cargo/bin/cargo install cargo-cacher
systemd-nspawn -D $CRATES_DIR --as-pid2 mv /home/$BUILD_USER/.cargo/bin/cargo-cacher /usr/local/bin
systemd-nspawn -D $CRATES_DIR --as-pid2 ln -s /lib/systemd/system/cargo-cacher.service /etc/systemd/system/multi-user.target.wants/cargo-cacher.service
systemd-nspawn -D $CRATES_DIR --as-pid2 mkdir /opt/cargo-cacher
cat <<EOF > cargo-cacher.service
[Unit]
Description=Cargo caching proxy
Documentation=https://github.com/ChrisMacNaughton/cargo-cacher

[Install]
WantedBy=multi-user.target

[Service]
ExecStart=/usr/local/bin/cargo-cacher -p 443 -i /opt/cargo-cacher

EOF
mv cargo-cacher.service crates-mirror/etc/systemd/system/
# Use the debian base as a template for our checkout dir, and create our build user
systemd-nspawn -D $CHECKOUT_DIR --template $BASE_DIR --as-pid2 useradd -m -s /bin/bash $BUILD_USER

#Created symlink /etc/systemd/system/dbus-org.freedesktop.network1.service → /lib/systemd/system/systemd-networkd.service.
#Created symlink /etc/systemd/system/multi-user.target.wants/systemd-networkd.service → /lib/systemd/system/systemd-networkd.service.
#Created symlink /etc/systemd/system/sockets.target.wants/systemd-networkd.socket → /lib/systemd/system/systemd-networkd.socket.
#Created symlink /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service → /lib/systemd/system/systemd-networkd-wait-online.service.

# Install dependencies in our checkout dir
systemd-nspawn -D $CHECKOUT_DIR --as-pid2 apt-get install -y \
  git curl gcc autoconf autopoint wget bison build-essential cmake file \
  rsync flex fuse genisoimage git gperf libc6-dev-i386 libfuse-dev libhtml-parser-perl \
  libpng-dev libtool m4 nasm pkg-config syslinux-utils texinfo qemu-system-x86 kitty-terminfo

# E.T. chown home
systemd-nspawn -D $CHECKOUT_DIR --as-pid2 chown $BUILD_USER:$BUILD_USER -R /home/$BUILD_USER

# Install Rust, xargo, and cargo-config
systemd-nspawn -D $CHECKOUT_DIR --as-pid2 -u $BUILD_USER --chdir=/home/$BUILD_USER \
  sh -c 'curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly'
systemd-nspawn -D $CHECKOUT_DIR --as-pid2 -u $BUILD_USER /home/$BUILD_USER/.cargo/bin/cargo install xargo cargo-config

# Clone redox in build user's home directory, with submodules
# Docs: https://gitlab.redox-os.org/redox-os/redox#cloning-building-running
systemd-nspawn -D $CHECKOUT_DIR --as-pid2 -u $BUILD_USER --chdir=/home/$BUILD_USER \
  git clone https://gitlab.redox-os.org/redox-os/redox.git --origin upstream --recursive

# Set up symlink: /etc/mtab -> ../proc/self/mounts
# This is required for our FUSE filesystem mounts during build
systemd-nspawn -D $CHECKOUT_DIR --as-pid2 ln -s ../proc/self/mounts /etc/mtab

# Clone the checkout to a build directory chroot, then make all; TODO generate this PATH var
systemd-nspawn --bind=/dev/fuse -D $BUILD_DIR --template $CHECKOUT_DIR --as-pid2 -u $BUILD_USER --chdir /home/$BUILD_USER/redox \
  sh -c "PATH=/home/$BUILD_USER/.cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin make all"

# After the build is done, you can inspect the artifacts on disk, since it's just a chroot.
#
# Start the container
#
#   # systemd-nspawn -D $BUILD_DIR --as-pid2 -u $BUILD_USER --chdir=/home/$BUILD_USER /bin/bash
#
# Inside the container
#
#   $ source .cargo/env           # (makefile wants to check your rust install)
#   $ cd redox
#   $ make qemu kvm=no vga=no     # kvm device is not not set up in our container, and a simple bind mount doesn't work
#                                 # see: https://github.com/systemd/systemd/issues/6648#issuecomment-325683022
#   Ctrl+A, X to quit qemu

# Bootable:
#   systemd-nspawn --network-zone=redox -b -D crates-mirror -p tcp:443:443
