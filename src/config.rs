use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use serde::{Serialize, Deserialize};

use anyhow::Error;
use toml;

#[derive(Serialize, Deserialize)]
pub struct Config {
     pub scratch_path: String,

     // Absolute path to cache_dir path for debootstrap
     pub debian_cache: String
}

impl Config {
    pub fn from_path(path: &PathBuf) -> Result<Self, Error> {
        let mut f = File::open(path)?;
        let mut s = String::with_capacity(1024);
        f.read_to_string(&mut s)?;
        let conf: Config = toml::from_str(&s)?;
        Ok(conf)
    }
}