use std::path::{Path, PathBuf};

use anyhow::Error;
use clap::{App, Arg, SubCommand};
use git;

use config::Config;
use container::DebianBase;

mod config;
mod container;

const VERSION: &'static str = "0.1.2";

fn main() -> Result<(), Error> {
    let base_path = PathBuf::from("/mnt/chemist");
    let app = App::new("chemist-runner").version(VERSION)
        .arg(Arg::with_name("config").long("config").short("c")
            .default_value("/opt/chemist-runner/etc/chemist-runner.toml")
        ).subcommand(
        SubCommand::with_name("init").about("initialize chemist-runner containers")
    );

    let matches = app.get_matches();
    let conf = Config::from_path(&PathBuf::from(matches.value_of("config").unwrap()))?;

    if let Some(_matches) = matches.subcommand_matches("init") {
        let db = DebianBase::build(&conf)?;
    }

    Ok(())
}


