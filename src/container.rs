use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

use anyhow::{anyhow, Error};

use crate::config::Config;
use std::io::{BufWriter, Write};

/// Base Debian image that we build other images from.
pub struct DebianBase {
    image: String
}

impl DebianBase {
    pub fn build(config: &Config) -> Result<DebianBase, anyhow::Error> {
        let image_name = "debian-base";
        if machinectl_image_exists(image_name)? {
            return Ok(DebianBase{image: image_name.to_owned()})
        }
        let scratch_path = PathBuf::from(&config.scratch_path);
        let cache_env_dir = scratch_path.join(image_name);
        let debian_cache = PathBuf::from(&config.debian_cache);
        if !debian_cache.is_absolute() {
            return Err(anyhow!("debian_cache must be absolute path"));
        }
        std::fs::create_dir_all(&debian_cache)?;

        if cache_env_dir.exists() {
            std::fs::remove_dir_all(&cache_env_dir)?;
        } else {
            std::fs::create_dir_all(&scratch_path)?;
        }

        // TODO(cm): detect arch
        let arch = "amd64";
        let extra_pkgs = "nginx,dbus";
        let mut child = Command::new("debootstrap")
            .args(vec!["--cache-dir", config.debian_cache.clone().as_str(),
                       "--include",
                       extra_pkgs,
                       "--arch", arch,
                       "stable",
                       cache_env_dir.to_str().ok_or(anyhow!("could not build cache-env path string"))?])
            .spawn()?;
        // TODO add timeout
        child.wait()?;
        machinectl_import_fs(cache_env_dir.to_str().unwrap(), image_name)?;
        Ok(DebianBase {image: image_name.to_owned()})
    }
}

struct CargoCache {
    image: String
}

impl CargoCache {
    pub fn build(base: &DebianBase) -> Result<CargoCache, anyhow::Error> {

        let image_name = "cargo-cache";
        if machinectl_image_exists(image_name)? {
            return Ok(CargoCache{image: image_name.to_owned()});
        }

        // chemist user
        create_chemist_user(image_name)?;

        // install rust
        install_rust(image_name)?;

        // copy cert and key

        todo!()
    }
}

fn create_chemist_user(machine: &str) -> Result<(), anyhow::Error> {
    let mut child = Command::new("systemd-nspawn")
        .args(vec!["-M", machine, "useradd", "-m"]).spawn()?;
    child.wait()?;
    let mut child = Command::new("systemd-nspawn")
        .args(vec!["-M", machine, "sh", "-c", "echo chemistry | chpasswd"]).spawn()?;
    child.wait()?;
    Ok(())
}

fn install_rust(machine: &str) -> Result<(), anyhow::Error> {
    let mut child = Command::new("systemd-nspawn")
        .args(vec!["-M", machine, "sh", "-c", "'curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain nightly'"])
        .spawn()?;
    child.wait()?;
    Ok(())
}

fn machinectl_image_exists(name: &str) -> Result<bool, anyhow::Error> {
    let status = Command::new("machinectl")
        .args(vec!["show-image", name])
        .stdout(Stdio::null())
        .status()?;
    Ok(status.success())
}

fn machinectl_import_fs(dir: &str, name: &str) -> Result<(), anyhow::Error> {
    let mut child = Command::new("machinectl")
        .args(vec!["import-fs", dir, name]).spawn()?;
    child.wait()?;
    Ok(())
}

